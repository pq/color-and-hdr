% SPDX-FileCopyrightText: 2021 Collabora, Ltd.
% SPDX-FileCopyrightText: 2022 Red Hat
% SPDX-License-Identifier: MIT

% This is an Octave script: https://www.gnu.org/software/octave/

s = 0.0031308;

o = [0 : 0.01 : 1];

e = zeros(size(o));

mask = o <= s;

e(mask) = o(mask) * 12.92;
e(~mask) = 1.055 * realpow(o(~mask), 1 ./ 2.4) - 0.055;

f = figure();
plot(o, e);
xticks([0:0.1:1])
yticks([0:0.1:1])
grid on
title('sRGB OETF (two-piece function)')
xlabel('optical / linear value')
ylabel('electrical / non-linear value')
axis square tight

ppi = get(0, 'ScreenPixelsPerInch');
set(f, 'PaperPosition', [0 0 400 400] ./ ppi)

print(f, 'sRGB_OETF.png', '-dpng')
