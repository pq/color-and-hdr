---
SPDX-FileCopyrightText: 2023 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[TOC]

[Front page](../README.md)


# The Compositor Color Management Model


## Abstract

This document explains the expected conceptual color management model to be
found inside Wayland compositors that support the color-management protocol
extension. It also discusses the role of the end user in getting the color
reproduction they desire.


## Introduction

[Wayland] is a display system protocol.  Therefore all images carried by
Wayland should have been produced for some display with all artistic intents
already rendered. The job of a Wayland compositor is only to convert an image
intended for one display and viewing conditions to another display and viewing
conditions when necessary ([color re-rendering]).

Such conversion may have several different goals (a.k.a rendering intents), but
the usual goal is to be faithful to the original artistic intent and perception
at the cost of exact colorimetry. This particular goal is called *the
perceptual rendering intent*. [International Color Consortium][ICC] (ICC)
defines also other rendering intents that are optionally supported in the
Wayland [color-management protocol extension][MR14].

[Wayland]: https://wayland.freedesktop.org
[color re-rendering]: https://cie.co.at/eilvterm/17-32-017
[ICC]: https://color.org
[MR14]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14


## The importance of viewing conditions

The human perception of color depends on everything seen, both in space
(the field of view) and time (adaptation over time). In the scenario
considered here, the main thing in the field of view is the display, but one
may also see the bezel of the display, the surrounds, and even reflections
on the display. One may temporarily look away from the display and see more
of the surrounds. Most notably, the surrounds may be bright, dim or dark.
All these things together are the viewing conditions, excluding the light
intentionally emitted (for emissive display, or reflected by a reflective
display) by the displayed image.

Consider the light emitted by a display with the intention of showing
an image. [Colorimetry](glossary.md#colorimetry) is an objectively measurable
property of light. However, the perception that light creates, its appearance,
is always relative to the viewing conditions. Therefore, concentrating only on
(display emission) colorimetry does not produce a predictable perception.
With entertainment like movies and games, the perception is important
while colorimetry is just the means to get there.

One notable thing affected by the viewing conditions is the adapted white:
what colorimetry appears "white", achromatic. In order to produce the
appearance of achromatic white or gray, you need a different mix of your
primary colors in different viewing conditions.

Reflections on a display are harmful to perceived image quality. Even a
completely uniform reflection on a display will increase the black
luminance, hiding details in dark areas and reducing the apparent saturation
and contrast.

Therefore standards like [Rec. ITU-R BT.2100][BT.2100] define not only the
colorimetry but also the reference viewing conditions. Actual viewing
conditions may differ from the reference viewing conditions, and that may
require mitigation in order to preserve the intended appearance.

[BT.2100]: http://www.itu.int/rec/R-REC-BT.2100


## Compositor color management model

The following diagram represents the Wayland compositor color management model.
The model explains how the different image descriptions and colorimetry and
viewing conditions fit into the image processing pipeline that takes an image
from an application (a Wayland client) and turns it into light through a
display.

![A diagram](images/wayland_color_management_model.svg.png "Compositor color management model")

This model is conceptual. Implementations may be more complex,
combine, split or re-order steps as long as the end result remains equivalent.
The naming of the steps suggests that the composition happens in optical
space, but implementations are free to choose otherwise.

Surface and output image descriptions are the same as in the
[color-management][MR14] Wayland protocol extension. The protocol also
carries surface's preferred image description, but that is not mentioned
here because it is just a recommendation to Wayland clients and freely
decided by a compositor.

The processing steps:

1. **Decode:** Electrically encoded client image is decoded into
[tristimulus values](glossary.md#tristimulus-values). The color space and
viewing conditions of the values are still defined by the surface image
description.

2. **Convert:** The tristimulus values are converted from the surface image
description to the compositor's blending space. The blending space has its
own assumed viewing conditions and a display definition which must be
accounted for in the conversion. The surface's rendering intent
is applied here. This conversion may depend on the output image description
as well, because the blending space may be derived directly from the output
image description.

3. **Compose:** A composition of all windows (surfaces) on screen is rendered,
with blending as necessary. This produces the single output image which is
still in the blending space.

4. **Encode:** The output image is electrically encoded for the output image
description. If the blending space viewing conditions and display definition
differ from the output image description, they must be accounted for first.
The encoding, colorimetry and viewing conditions of the result are defined by
the output image description.

5. **Adjust:** Extra end user image adjustments may be applied here. Mostly
these adjustments are to work around the lack of sufficient adjustability
in the monitor and should be avoided otherwise. The traditional VCGT fits here,
but the adjustments do not need to be that coarse and limited.

6. **Decode:** The monitor (or TV) decodes the incoming video signal according
to its input signal specification. The source of the video signal might affect
the assumed signal specification with signal metadata (e.g. HDMI InfoFrames).

7. **Adjust:** Most monitors offer some knobs for end users, like brightness
and contrast, for them to adjust the image to their actual viewing conditions
and preferences. Monitors may have sensors to automate some adjustments.
Monitors might do adjustments in order to differentiate between monitor
vendors. Adjustments could also be dynamic, based on the input signal contents.

8. **Emit:** The signal controls the light emissions of the pixels in a
display panel. This results in the actual light colorimetry which is then
perceived in the context of the actual viewing conditions.

The pixel values of image content are given a meaning through the
*surface image description* which defines the colorimetry of the pixels
and the viewing conditions where that colorimetry produces the intended
appearance.

Compositor's *blending colorimetry and viewing conditions* are the common
color space into which all image content from applications is converted to.
The composited output image must be described by a single colorimetry and
viewing condition definition, it cannot have different definitions in
different parts. Also blending requires input values that are in the same
space, otherwise its result would be like saying "I added 3 meters to 4
kilograms and got 7" - nonsense.

*Output image description* characterizes the display and the viewing conditions
that the compositor is targeting. This should be the same signal colorimetry
and viewing conditions that the display (monitor or TV) is expecting to
receive, but sometimes they may need to differ. Note, that output image
description often does not describe the actual light colorimetry nor the
actual viewing conditions because of step 7. This is an important
distinction for applications that may have specific expectations about light
colorimetry - more of that in [How to reproduce specific colorimetry].

[How to reproduce specific colorimetry]: #how-to-reproduce-specific-colorimetry

A display can be thought of always having its
*expected colorimetry and viewing conditions*. This is how the display will
interpret its input video signal, and the expectations may be explicit or
implicit. E.g. [ANSI/CTA-861-H][CTA-861-H] has several such definitions pointing
to SMPTE and ITU documents. It also defines how the display capabilities are
found in EDID, and how the video source indicates which definition it has
chosen. Even the computer monitor "default RGB" colorimetry has its expectations
expressed through EDID, optionally referring to IEC 61996-2-1 (the sRGB
standard).

The characteristics of the expected video signal rarely match with the
*actual light colorimetry and viewing conditions* completely. At the very
least the viewing conditions tend to be different, which calls for the
brightness, contrast, color temperature and other adjustment knobs in monitors.
End users are expected to use the monitor knobs to make the image look as
good as possible in their actual viewing conditions.

When the monitor knobs are not sufficient, step 5 adjustments may be necessary.
This creates a discrepancy between the compositor produced colorimetry and
the output image description. The video signal characteristics are altered in
order to get the monitor to produce the desired appearance.

[CTA-861-H]: https://shop.cta.tech/products/a-dtv-profile-for-uncompressed-high-speed-digital-interfaces-cta-861-h

### Colorimetric rendering intents (ICC)

If an application chooses one of the ICC colorimetric rendering intents,
then the compositor's conversion in step 2 shall follow the ICC definition.
This produces the ICC expected colorimetry, converted from the surface image
description to the output image description. However, then it depends on the
calibration how the computed colorimetry is mapped to light colorimetry.

If the output image description is based on a measured profile and the
viewing conditions are recorded appropriately, then the emitted light will
have the ICC expected colorimetry. This does not require the application to
manage its own colors, that is, to set the output image description on its
surfaces and do all the color computations itself.

A major caveat here is that if the monitor does sensor-based or dynamic
adjustments (step 7), the calibration may not be stable. That makes any
measured profile practically useless, and colorimetrically accurate color
reproduction is essentially impossible. This is popular behavior in TVs
for instance.

ICC colorimetric rendering intents are not limited to ICC profiles. The same
principles can be used with all types of image descriptions.

### Compositor color effects

Examples of compositor color effects include
graying out inactive or non-responsive windows, and contrast enhancement
(e.g. for visually impaired).

Compositor color effects should logically be part of step 5, even though they
might be implemented in steps 2 or 4 in practice. This means that output
image description does not describe those effects, hence applications cannot
opt out of them by targeting the output image description directly.

This also makes the compositor color effects essentially part of the color
calibration of the whole display system. The following terms are proposed:
**locked** and **fixed** calibration.

If the calibration is *locked* by the
end user in compositor settings, compositor color effects are forbidden.
If the calibration is *fixed* but not locked, compositors may add color effects,
but they also need to indicate through color-management protocol that
color reproduction is uncalibrated or compromised as long as color effects
apply.

### Screenshooting and recording

Screenshooting, window and desktop recording interfaces should define the
image description used for the produced imagery. It could be a fixed definition
by the recording interface specification, it could be chosen by the
compositor, or it could be provided by the application. Compositors must
appropriately convert the imagery to follow that image description.

A straightforward implementation would be to consider a recording as a
temporary virtual output with its own image description as defined by the
recording interface. As calibration is per output, whether that is inside a
monitor or applied by the compositor, the recording "output" would have its
own calibration. The calibration for virtual outputs should be the identity
transformation.


## How to reproduce specific colorimetry

Some use cases require reproducing the exact pixel colorimetry in light,
and avoid all perceptual enhancements. The only way to ensure that is to
fix *calibration* and then measure the display light response to produce a
display profile. This assumes that the display light response is predictable
which might not be the case with consumer HDR displays.

The color-management protocol has no programmatic way for applications to ask
for actual light colorimetry. This is always up to the end user to choose
display hardware that is predictable, choose the calibration appropriately,
acquire the necessary display profile possibly through physical measurements,
set up the profile and the output image description according to their
intentions, and verify that the result is as expected.

An application can counter-act all compositor's automatic perceptual and other
color conversions by tagging its surfaces with the output image description
advertised by the compositor. Provided that the surface is not blended with
anything, its RGB pixels will come out of steps 1-4 unchanged. Depending on
calibration, which is controlled by the end user, step 5 should be a
no-operation. This creates an identity path from application to video signal.
The identity path can be used for both display measurements and application
managed color.

If the display profile is set as the output image description, it will
effectively undo all the adjustments done in steps 5 and 7. It makes the compositor
target the actual light colorimetry. If a display profile does not include
viewing conditions, they have to be somehow invented for the output image
description. Ideally, those would be the actual viewing conditions and
the compositor's automatic perceptual conversions would please the end user.
Aside from tweaking the viewing conditions in the output image description,
this leaves little room for end user adjustments regardless of application
content.

An alternative is to use e.g. a standard image description, or the
manufacturer's display profile with added viewing conditions, use monitor
knobs to adjust compositor managed color to be pleasing, lock the calibration,
measure the display response, and use the measured display profile in
application managed color only. Then application managed color can emit the
exact colorimetry, while all other applications, including HDR, should look
pleasing.
