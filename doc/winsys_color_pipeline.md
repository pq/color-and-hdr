---
SPDX-FileCopyrightText: 2020 Collabora, Ltd.
SPDX-License-Identifier: MIT
---

Contents

[TOC]

[Front page](../README.md)


# Color Pipeline Overview

Even though the reader may be well familiar with how color management works in a
X11 graphics stack, it is still useful to review that before introducing the
Wayland color management architecture, and then compare the two. The
architecture design between X11 and Wayland color management is
fundamentally different. They also differ in what is considered as
calibration.

Calibration in this document is taken to mean all those settings and parameters
affecting the reproduction of colors on a monitor the user has to take care of
manually. This includes things like monitor adjustments you make with the setup
menu or buttons on a monitor itself. Other things may also need to be taken care
of manually and that is where the X11 and Wayland software architectures differ.

This overview does not consider creating or measuring color profiles.
That is a topic to be discussed in another chapter. Here it is assumed
that appropriate monitor color profiles are already available.

The output color space here refers to the color space and transfer function used
for the final framebuffer content, or more precisely, the electrical (digital)
signal to be transmitted through the wire to a monitor. The monitor imposes its
own EOTF when converting the signal into actual light.


## X11 color pipeline

The X11 color pipeline is shown in **Figure 1**. In this model, the
display server is completely agnostic of any color management happening.
The display server's job is to stay out of the way while X11 clients do
all their color reproduction on their own, perhaps with the help of a CMM.

![X11 color pipeline diagram](images/color_pipeline_x11.svg.png "Figure 1")

> **Figure 1**: X11 color pipeline. Color management happens primarily in the
> clients. All display server color settings are considered to be included in
> calibration.

This model views the display server as part of the display, something that
passively shows your images as is. Therefore all the display server settings
that affect color (gamma settings, color *look-up tables* (LUT)) are considered
to be part of the display calibration. A monitor ICC profile file may contain a
[VCGT] tag with the LUT values that something needs to load into one of the LUTs
of the display server. The monitor color profile recorded in an ICC file is not
valid if the VCGT tag is not applied as intended.

Keeping a monitor color profile valid depends on keeping the calibration fixed.
With X11 this is quite fragile, because all X11 clients are allowed to change
any color settings in the display server at any time, without notice and
without user interaction. It is mostly up to the user to avoid running programs
that touch those settings, except the one program that sets up the correct
calibration. Old games often play with gamma settings, and some applications
are specifically built to change colors like [Redshift].

Not only are all X11 clients able to change your color settings behind your
back, but there are actually several different settings for more or less the
same thing. You can set a global gamma factor in your `xorg.conf`.
[XFree86-VidModeExtension] allows to control gamma ramps via parameters and as
LUTs. [RandR] extension added per-output LUTs. Before [Xorg 1.19] the latest
setting more or less overwrote all the others, but starting from 1.19 all these
settings are combined to produce the final LUT ([commit][xserver-lut-combine]).
There is also a proposal to add even more tunables ([MR352]). It may also be
possible to change the monitor behavior through [RandR].

**Figure 1** depicts an X11 server with only one `Screen` in the protocol but
two independent outputs (monitors). If applications need to use a different
monitor profile for each output, they have to watch their window position,
detect which output it is on, and repaint their window with the right profile.
The advantage of this is that the application knows exactly (if it is smart
enough to detect it) which parts of the window show on which output. If the two
monitors were setup as clones then the application is forced to pick just one
monitor profile.

[VCGT]: http://www.argyllcms.com/doc/calvschar.html
[Redshift]: http://jonls.dk/redshift/
[XFree86-VidModeExtension]: https://cgit.freedesktop.org/xcb/proto/tree/src/xf86vidmode.xml
[RandR]: https://gitlab.freedesktop.org/xorg/proto/xorgproto/-/blob/master/randrproto.txt
[Xorg 1.19]: https://lists.x.org/archives/xorg-announce/2016-November/002737.html
[xserver-lut-combine]: https://gitlab.freedesktop.org/xorg/xserver/-/commit/b4e46c0444bb09f4af59d9d13acc939a0fbbc6d6
[MR352]: https://gitlab.freedesktop.org/xorg/xserver/-/merge_requests/352


## Wayland color pipeline

The fundamental difference in the Wayland color pipeline (**Figure 2** compared
to the X11 color pipeline (**Figure 1**) is that the display server is an active
participant in color management. The display server (a Wayland compositor)
automatically converts from a client provided color space to an output color
space as necessary. The compositor is the primary user of a CMM, although
clients can use a CMM to prepare their content as well.

A Wayland client (application) tells the compositor what color space its
content is in. Knowing also the monitor color profile, the compositor (with the
help of a CMM) can do the necessary color conversion separately for each
monitor. Even when monitors are cloned, each monitor can have its own arbitrary
color profile. A client does not necessarily need to react at all when its
window is moved from one monitor to another to maintain good color
reproduction.

**Figure 2** roughly depicts how this works. A compositor uses a CMM to compute
the necessary color space transformations based on the client provided content
description and the monitor profile.  CSC1 is the color space conversion from
client content color space to the blending space, and CSC2 is the conversion
from blending space to output space for the particular monitor. CSC1 may be
different for each application window. CSC2 depends on the chosen blending space
and the monitor color profile.

The compositor blending color space should use a numerical encoding that is
linear in luminance and has a suitably wide gamut, preferably unbounded. One
potential blending space is the output color space with its EOTF applied. The
blending space can be different for each monitor. The actual choice of color
space is a compositor implementation detail.

A client has the possibility to deliver content already in the output color
space. In that case, assuming the pixels from the client are unoccluded and not
blended with anything else, the color space conversion applied by the
compositor on that particular output is identity, up to computation and monitor
wire format precision. This feature can also be used creatively by an
application claiming to deliver content in the output color space but instead
use a different profile internally in preparing its image content.

For simplicity this section is using the term color space to describe the color
properties of an image. One goal of compositors can be to retain the appearance
of the colors as closely as possible. To make this possible on arbitrary
displays and in arbitrary viewing environments, more than just the colorimetry
of the image has to be available to compositors. The color management protocol
bundles the information in an image description object which replaces the
simpler concept of a color space.

![Wayland color pipeline diagram](images/color_pipeline_wayland.svg.png "Figure 2")

> **Figure 2**: Wayland color pipeline. Color management is primarily the
> compositor's responsibility while the clients merely describe their content's
> color properties.


## Wayland and Calibration

Calibration in the Wayland model considers only the monitor settings. Video
card properties, including "the LUT", are controlled by the compositor itself
and they are never exposed for clients to set directly. Therefore video card
properties do not need to be considered as calibration. Instead, video card
properties can be used by the compositor to off-load color space conversions to
the hardware as it sees fit and at any time. Modern video cards have more
flexibility (de-gamma LUT, color transform matrix, gamma LUT; sometimes some of
these are found on hardware planes before and/or after plane blending) than
just one LUT, and making the most of them is really only possible if it is done
by the compositor automatically, frame by frame.

If a color profile is given as an ICC file with a [VCGT] tag set, the color
profile contained in that file is not valid unless the LUT encoded in the VCGT
tag is applied (why else would the profile contain the tag?). Hence,
Wayland compositors need to apply the VCGT tag if it exists, but in this case
it is merely yet another transformation in the abstract color pipeline rather
than something to be loaded directly into hardware.

A compositor may also be able to change the monitor behaviour. AVI InfoFrames
may be able to change what color space the monitor is expecting data in, for
instance. This still counts as calibration, as the change would invalidate a
color profile measured with another monitor setting. More traditional knobs
(brightness or backlight, contrast, etc.) may be software controllable as well.
The intention is that the compositor has exclusive access to these knobs and is
able to maintain and enforce calibration.

Since the compositor in use is intended to have exclusive access to all
software-controllable calibration settings, there is no risk that applications
would be able to corrupt the calibration. For use cases where calibration is
enough and a (custom) monitor color profile is not necessary, the compositor
can switch the calibration on-demand. For example, when showing video content
in fullscreen, a compositor may tell the monitor or TV to switch to a better
suited color mode. It is up to compositor policy and user preferences to
determine when that is appropriate.

For general information on calibration versus profiling, see [Elle Stone].

[Elle Stone]: https://ninedegreesbelow.com/photography/monitor-profile-calibrate-confuse.html
