---
SPDX-FileCopyrightText: 2024 Red Hat, Inc.
SPDX-License-Identifier: MIT
---

Contents

[TOC]

[Front page](../README.md)


# Mastering Display Color Volume (MDCV) and Target Color Volume

There are a few places in which the [SMPTE ST 2086 Mastering Display Color
Volume metadata][ST-2086] shows up. Displays accept [HDR Static Metadata
InfoFrames][CTA-861], Vulkan has the [`VK_EXT_hdr_metadata`] extension, Wayland's
color-management protocol and various video formats encode this information as
well.

To understand the purpose of the metadata, one has to understand the problem it
intends to solve.

With display capabilities increasing and being able to produce bigger color
volumes, the need for new video encoding standards which are able to make use of
the new capabilities were needed. The [BT.2020] primaries and the [PQ transfer
characteristics][ST-2084] were developed for this purpose. The [color volume]
reachable by the encoding was designed to be much larger than what current
displays are capable of in the name of future-proofing.

Displaying [colorimetry] of this [encodable color volume] on actual displays
becomes a problem. If one has to assume that an image in this encoding can
contain any colorimetry contained in the entire encodable color volume, the
colorimetry can fall out of the color volume of an actual display.

The way to deal with those out-of-gamut colors is gamut mapping. A component in
the display chain applies a function to the input and maps it into the display
color volume. A basic mapping function is clipping. This often results in color
banding and the greater the distance of the colorimetry to the actual display's
color volume, the worse this effect gets. There are more sophisticated mapping
functions but they all do worse with greater distances.

To minimize the loss in quality created by the mapping function, it's important
to minimize the distance to map. Some strategies create histograms and do other
analyzes on the image to do so, but this is computationally expensive. Some
strategies, generally referred to as Dynamic HDR Metadata, involve the content
carrying pre-defined color transforms to map from the encoding to multiple other
pre-defined color volumes which are potentially closer to the actual display's
color volume.

Another strategy is to realize that the content was created on an actual display
which also has a smaller color volume than what the encoding supports. The
Mastering Display Color Volume metadata is an description of this display.

In other words, this metadata is a tool to shorten the mapping distance of
colorimetry between the content and the actual display at hand.

We also use the term [target color volume] to describe the Mastering Display
Color Volume. The target color volume can also describe the color volume of the
actual display at hand.

With this background in mind, the SMPTE ST 2086 specification and some [further
information given by the SMPTE][SMPTE-annex] a few observations can be made:

- The Mastering Display Metadata is optional. When no metadata is given,
  everything is still well-defined. The only consequence is a possible loss in
  image quality due to the greater mapping distances.

- The Mastering Display Metadata is a color volume. It requires three primaries
  (R, G, B) and a white point (W) to define its shape. All those parameters are
  taken, without any transform, from the mastering display.

- The Mastering Display Metadata does not contain a viewing environment
  definition. The viewing environment of the mastering display is defined by the
  color encoding standard.

- Mastering displays are supposed to clip. Only colorimetry in the encoding that
  falls into the mastering display color volume has an intended reproduction.
  For technical reasons such as compression, quantization errors etc, some
  colorimetry might exceed the mastering color volume slightly.

- Any component in the display chain can thus clip the colorimetry to the
  mastering color volume and preserve the intended reproduction.

- It is important for the Mastering Display Metadata to be correct because of
  the previous points. If no sensible values are available, it is better to omit
  sending or encoding the metadata.

[ST-2086]: specs.md#smpte-st-20862018
[CTA-861]: specs.md#ansicta-861-i
[`VK_EXT_hdr_metadata`]: https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VK_EXT_hdr_metadata.html
[BT.2020]: specs.md#itu-r-bt2020
[ST-2084]: specs.md#smpte-st-20842014
[color volume]: glossary.md#color-volume
[colorimetry]: glossary.md#colorimetry
[encodable color volume]: glossary.md#container-color-volume
[target color volume]: glossary.md#target-color-volume
[SMPTE-annex]: ../annex/smpte_st2086_observation_and_guidelines.docx
