#!/usr/bin/env sh

set -eu

arg="${1:-all}"

dir=$(dirname -- "$( readlink -f -- "$0"; )";)
cd $dir

markdown_files=$(find . -name '*.md' -not -path '*/.*')
image_markdownlint=ghcr.io/igorshubovych/markdownlint-cli:v0.39.0
image_markdown_link_check=ghcr.io/tcort/markdown-link-check:3.10
image_vale=docker.io/jdkato/vale:v3.3.1

podman="podman run --user root --rm \
          -v ${dir}:/color-and-hdr:Z \
          --workdir=/color-and-hdr"

markdownlint=$(command -v markdownlint || \
               echo "${podman} ${image_markdownlint}")

markdown_link_check=$(command -v markdown-link-check || \
                      echo "${podman} ${image_markdown_link_check}")

vale=$(command -v vale || echo "${podman} ${image_vale}")

if [ "$arg" = "markdown" ] || [ "$arg" = "all" ]; then
  set -x
  $markdownlint \
    -c ./.markdownlint.yml \
    -r ./.markdownlint-rules.js \
    $markdown_files || exit 1
  set +x
fi

if [ "$arg" = "links" ] || [ "$arg" = "all" ] ; then
  set -x
  $markdown_link_check -q \
    -c .gitlab-ci/markdown-link-checker-config-local-only.json \
    $markdown_files || exit 1
  set +x
fi

if [ "$arg" = "all-links" ] ; then
  set -x
  $markdown_link_check $markdown_files || exit 1
  set +x
fi

if [ "$arg" = "style" ] || [ "$arg" = "all" ] ; then
  set -x
  $vale sync
  $vale $markdown_files || exit 1
  set +x
fi