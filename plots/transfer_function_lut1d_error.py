#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Collabora, Ltd.
# SPDX-License-Identifier: MIT

# This script is used for comparing the analytical formula of an
# encoding (color component) transfer function to its approximation with
# a 1D LUT. Below you can choose which encoding function to study, and
# how many uniformly distributed taps the linearly interpolated look-up
# table has. You can also divide the electrical axis to a number of
# quantization levels.
#
# Try setting both num_taps and num_quant_levels to 256, and then zoom
# in near zero in the graph. Observe how the LUT approximation differs
# from the analytical result, and how low optical values get squashed into
# too dark electrical values.
#
# The precision problem is characteristic to pure power-law functions
# with the exponent less than 1.0, which is typical for display inverse
# EOTFs.
#
# Notice that the error scale is on the right hand side of the graph,
# while output values are on the left hand side y-axis.

import colour
import numpy as np
import matplotlib.pyplot as plt

# The name of the transfer function to encode for.
tf_name = 'Gamma 2.2'

# You can find the available transfer function names by uncommenting this line:
#print(sorted(colour.CCTF_ENCODINGS.keys()))

# Our transfer function to inspect.
# The input is optical values, output is electrical values.
# If the transfer function you chose has parameters, you can add them in the
# cctf_encoding() call as kwargs.
def transfer_function(x):
	return colour.cctf_encoding(x, tf_name)

# How many taps to have in the LUT
num_taps = 7

# Number of quantization levels in electrical values, for plotting them.
# Zero to disable.
num_quant_levels = 16

# Instantiate the tap positions.
taps = np.linspace(0.0, 1.0, num_taps)

# Compute LUT values, the naive way.
lut = transfer_function(taps)

# The test points, 50 points per linear segment to have good coverage.
x = np.linspace(0.0, 1.0, 50 * num_taps)

# The reference results.
ref = transfer_function(x)

# 1D LUT results, linearly interpolated.
intp = np.interp(x, taps, lut)

# error analysis
err = intp - ref
maxerr = max(abs(err))

titl = f"Encoding for {tf_name}: original vs. 1D LUT with {num_taps} taps"
print(f"{titl}, error:\n"
	f"\tmax abs {maxerr:.6g} ({maxerr * 255:.2g} / 255),\n"
	f"\tmean {np.mean(err):.4g} stddev {np.std(err):.4g}")

# plotting
p = []
fig, ax1 = plt.subplots()
ax1.set_title(titl)

p += ax1.plot(x, intp, 'k-', linewidth=1, label='LUT approximated')
p += ax1.plot(x, ref, 'g:', linewidth=3, label='original')

ax2 = ax1.twinx()
p += ax2.plot(x, err, 'r-', linewidth=1, label='error')

if num_quant_levels > 0:
	# quantized electrical values for plotting
	qe = np.linspace(0.0, 1.0, num_quant_levels)

	# corresponding optical values
	qo = np.interp(qe, lut, taps)

	h = [ax1.plot([0, qo[i], qo[i]], [qe[i], qe[i], 0], 'b+--', label='quantization', linewidth=0.5, mew=0.5)
		for i in range(len(qe))]
	p += h[0]

ax1.legend(handles=p, loc='center right', shadow=True)
ax1.set_xlabel('Optical')
ax1.set_ylabel('Electrical')
ax2.set_ylabel('error', color='r')
ax2.tick_params(axis='y', color='r', labelcolor='r')

plt.show()
