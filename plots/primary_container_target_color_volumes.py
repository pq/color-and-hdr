#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023 Red Hat, Inc.
# SPDX-License-Identifier: MIT

# This script is used for visualizing the primary, container and target color
# volumes. The volumes are projected on the chromaticity diagram but the idea
# of the different volumes becomes clear from the 2D projection already.

import colour
import matplotlib.pyplot as plt
import numpy as np

bt709 = colour.RGB_COLOURSPACES['ITU-R BT.709']
bt2020 = colour.RGB_COLOURSPACES['ITU-R BT.2020']
target_cs = colour.RGB_COLOURSPACES['DCI-P3']

primary = { 'label': 'primary color volume', 'color': 'red', 'linewidth': 3, 'linestyle': '-' }
container = { 'label': 'container color volume', 'color': 'blue', 'alpha': 0.5 }
target = { 'label': 'target color volume', 'color': '#20ff20', 'linewidth': 3, 'linestyle': ':' }

fig, axs = plt.subplots(2, 2, figsize=(8.5, 10))

# bt709 primary + container for integer formats
ax1 = axs[0, 0]
colour.plotting.diagrams.plot_chromaticity_diagram_CIE1931(standalone=False, diagram_opacity=0.3, axes=ax1)
P = bt709.primaries
P_p = np.vstack([P, P[0]])
ax1.plot(P_p[..., 0], P_p[..., 1], primary['color'], linewidth=primary['linewidth'], label=primary['label'])
ax1.fill(P_p[..., 0], P_p[..., 1], container['color'], alpha=container['alpha'], label=container['label'])
ax1.legend()
ax1.set_title('BT.709 primaries and normalized integer formats')

# bt709 primary + container for floating point formats
ax2 = axs[0, 1]
colour.plotting.diagrams.plot_chromaticity_diagram_CIE1931(standalone=False, diagram_opacity=0.3, axes=ax2)
ax2.plot(P_p[..., 0], P_p[..., 1], primary['color'], linewidth=primary['linewidth'], label=primary['label'])
ax2.fill([0,0,1,1], [0,1,1,0], container['color'], alpha=container['alpha'], label=container['label'])
ax2.legend()
ax2.set_title('BT.709 primaries and floating point formats')

# bt2020 primary + target
ax3 = axs[1, 0]
colour.plotting.diagrams.plot_chromaticity_diagram_CIE1931(standalone=False, diagram_opacity=0.3, axes=ax3)
P = bt2020.primaries
P_p = np.vstack([P, P[0]])
ax3.plot(P_p[..., 0], P_p[..., 1], primary['color'], linewidth=primary['linewidth'], label=primary['label'])
ax3.fill(P_p[..., 0], P_p[..., 1], container['color'], alpha=container['alpha'], label=container['label'])
P = target_cs.primaries
P_p = np.vstack([P, P[0]])
ax3.plot(P_p[..., 0], P_p[..., 1], target['color'], linewidth=target['linewidth'], linestyle=target['linestyle'], label=target['label'])
ax3.legend()
ax3.set_title('BT.2020 primaries, normalized integer formats and DCI-P3 target color volume')

# bt709 primary + target
ax4 = axs[1, 1]
colour.plotting.diagrams.plot_chromaticity_diagram_CIE1931(standalone=False, diagram_opacity=0.3, axes=ax4)
P = bt709.primaries
P_p = np.vstack([P, P[0]])
ax4.plot(P_p[..., 0], P_p[..., 1], primary['color'], linewidth=primary['linewidth'], label=primary['label'])
ax4.fill([0,0,1,1], [0,1,1,0], container['color'], alpha=container['alpha'], label=container['label'])
P = target_cs.primaries
P_p = np.vstack([P, P[0]])
ax4.plot(P_p[..., 0], P_p[..., 1], target['color'], linewidth=target['linewidth'], linestyle=target['linestyle'], label=target['label'])
ax4.legend()
ax4.set_title('BT.709 primaries, floating point formats and DCI-P3 target color volume')

plt.show()
