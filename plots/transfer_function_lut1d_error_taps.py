#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Collabora, Ltd.
# SPDX-License-Identifier: MIT

# When an analytical formula is approximated with a 1D LUT, some error is
# introduced. This script studies the effect of the number of taps in a 1D
# look-up table vs. the maximum absolute error found over the curve. You
# can choose which transfer function to study, and what numbers of taps to
# test. The result is shown as a graph.
#
# Encoding for Gamma 2.2 is one of the most difficult functions, being a pure
# power-law function with an infinite slope at zero. To understand why,
# see the transfer_function_lut1d_error.py script.
#
# The maximum absolute error discovered here is trying to find the maximum
# over all input values. In real applications though, input values are more or
# less quantized, which means that with enough taps the maximum error may not
# be hit. However, for inverse EOTF, the input value is an optical value
# that may be a result from another EOTF, color space conversion and/or
# blending, which makes the quantization at the inverse EOTF input
# unpredictable.

import colour
import numpy as np
import matplotlib.pyplot as plt

# The name of the transfer function to encode for.
tf_name = 'Gamma 2.2'

# You can find the available transfer function names by uncommenting this line:
#print(sorted(colour.CCTF_ENCODINGS.keys()))

# Our transfer function to inspect.
# The input is optical values, output is electrical values.
# If the transfer function you chose has parameters, you can add them in the
# cctf_encoding() call as kwargs.
def transfer_function(x):
	return colour.cctf_encoding(x, tf_name)

# Sequence of number of taps to iterate through.
num_taps = range(17, 1400, 3)

# -----------------------------------------------------------

maxerr = []

for count in num_taps:
	# Instantiate the tap positions.
	taps = np.linspace(0.0, 1.0, count)

	# Compute LUT values, the naive way.
	lut = transfer_function(taps)

	# The test points, 50 points per linear segment to have good coverage.
	x = np.linspace(0.0, 1.0, 50 * count)

	# The reference results.
	ref = transfer_function(x)

	# 1D LUT results, linearly interpolated.
	intp = np.interp(x, taps, lut)

	# maximum absolute error
	maxerr.append(max(abs(intp - ref)))

titl = f"Error of approximating the encoding for {tf_name} with a 1D LUT"

fig, ax = plt.subplots()
ax.set_title(titl)

p = []
p += ax.plot(num_taps, maxerr, 'r-', label='Approximation error')
p += ax.plot([num_taps[0], num_taps[-1]], [1/255, 1/255], 'b-', label='1 / 255 level')

ax.legend(handles=p, loc='upper right', shadow=True)
ax.set_xlabel('Number of taps in 1D LUT')
ax.set_ylabel('Maximum absolute error')
ax.set_ylim(ymin=0.0)

plt.show()
