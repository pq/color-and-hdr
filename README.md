<!-- markdownlint-disable SPDX-front-matter front-page -->

# Color management and HDR documentation for FOSS graphics

Documentation in [this repository] is intended to help with the design and
implementation of color management and HDR support on FOSS graphics stacks,
including Mesa (EGL, Vulkan WSI), Linux (DRM KMS), Wayland (compositors and
applications), and even X11.

This is not an archive of proprietary documents like SMPTE, ITU, or VESA
specifications. All content must follow the [license].

[this repository]: https://gitlab.freedesktop.org/pq/color-and-hdr
[license]: LICENSE


## Contents

- [How to learn about digital color] is a walkthrough of free web resources
  explaining what digital color is and what color management does. This is
  intended for people who have zero background on these topics.

- [Wayland Color Management and HDR Design Goals] describes the expectations and
  use cases that Wayland should meet.

- [Color Pipeline Overview] compares the X11 and Wayland color pipelines, and
  explains how a Wayland compositor relates to display calibration.

- [Compositor color management model] goes into more detail how color-management
  protocol integrates with compositor color management and monitor adjustments.

- A list of relevant [specifications].

- How to build a [HDR10 system using Wayland].

- Our [Glossary].

- [A Pixel's Color] is an introduction to understanding pixel values and color.

- [Mastering Display Color Volume (MDCV) and Target Color Volume] gives some
  background on the issue it is trying to solve, points out observations and
  consequences, and what this all means in practice.

- [Tools] contains a list of tools that can be used to inspect and modify color
  management and display attributes on Linux.

- [Power] talks about the power implications of color-managed and HDR.

- [Notes on CICP] defined in Rec ITU-T H.273 *Coding-independent code points for
  video signal type identification*.

- [Wayland color protocols Q&A] Questions and Answers about the Wayland
  color-representation and color-management protocols.

- [Samples] has a list of links to sample files for videos, images and profiles.

- [Plots] has a list of Python scripts that plot various things, source code and
  example output included.

[How to learn about digital color]: doc/learn.md
[Wayland Color Management and HDR Design Goals]: doc/design_goals.md
[Color Pipeline Overview]: doc/winsys_color_pipeline.md
[Compositor color management model]: doc/color_management_model.md
[specifications]: doc/specs.md
[HDR10 system using Wayland]: doc/hdr10.md
[Glossary]: doc/glossary.md
[A Pixel's Color]: doc/pixels_color.md
[Mastering Display Color Volume (MDCV) and Target Color Volume]: doc/mastering_display_metadata.md
[Tools]: doc/tools.md
[Power]: doc/power.md
[Notes on CICP]: doc/cicp_h273.md
[Wayland color protocols Q&A]: doc/wayland_qa.md
[Samples]: doc/samples.md
[Plots]: plots/index.md


## History

Originally this documentation was started to better explain how [Wayland color
management extension][MR14] should be used and what it means.

Widening community interests particularly in HDR prompted for moving the
documentation into this separate repository to allow a more streamlined way of
contributing to it.

For now, this project lives in a personal space, but if it gets more traction,
moving it into an independent GitLab group is possible without losing any Issues
or MRs.

[MR14]: https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/14


## Releases

No releases are made from this repository. Use date or the git hash to
refer to specific revisions of the contents.


## Contributing

Open Issues and Merge Requests in GitLab as usual. Use your own forked
repository for MR branches (@pq is exempt as long as this repository is hosted
in their personal GitLab group). Each commit must carry Signed-off-by tag to
denote that the submitter adheres to [Developer Certificate of Origin 1.1].

All merge requests need to be accepted by at least one other person with
Developer or higher access level.

Reporter level access can be given by invite without any particular
requirements.

Developer access can be given on request, provided the person is actively
participating in discussions and has contributed an accepted MR.

Maintainer access is given on Maintainers' collective discretion.

### Formatting

New content should use [GitLab Flavored Markdown (GLFM)][GLFM]. We try to stick
to a few formatting rules, but they can be broken when there is a good reason to
do so:

- the same header everywhere (SPDX information, a link to the front page and a
  Table of Content where appropriate)
- 80 characters line limit
- two newlines before headings of level 1 or 2, a single newline otherwise
- use of reference-style links where appropriate to not break the flow
- link references always at the end of a section

We enforce those Markdown formatting rules using a linter, use Vale to find
issues with prose and a link checker to make sure all links are reachable.

To check for any issues locally, install `podman` and run the `run-check.sh`
script.

[GLFM]: https://docs.gitlab.com/ee/user/markdown.html

### Images

Please, also contribute the "sources" (e.g. SVG) of images if reasonable, even
when the image used by the document is an exported one, e.g. PNG.

### Writing mathematics

If you want to typeset mathematics, use [GLFM] and its [mathematics support]. It
uses [KaTeX] which supports [a subset of LaTeX].

[KaTeX demo] may be helpful for polishing your markup without needing to push to
and view in GitLab all the time.

[Developer Certificate of Origin 1.1]: https://developercertificate.org/
[mathematics support]: https://gitlab.freedesktop.org/help/user/markdown#math
[KaTeX]: https://katex.org/
[a subset of LaTeX]: https://katex.org/docs/supported.html
[KaTeX demo]: https://katex.org/#demo


## Conduct

This project follows [the freedesktop.org Contributor Covenant][CodeOfConduct].

[CodeOfConduct]: https://www.freedesktop.org/wiki/CodeOfConduct
