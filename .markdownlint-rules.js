"use strict";

module.exports = [{
  "names": [ "CHDR01", "SPDX-front-matter" ],
  "description": "There should be a front matter with the SPDX information",
  "tags": [ "color-and-hdr" ],
  "parser": "micromark",
  "function": function CHDR01(params, onError) {
    const lines = params.frontMatterLines;
    const spdxCopyrightRe =
      new RegExp(String("^SPDX-FileCopyrightText:"), "");
    const spdxLicensetRe =
      new RegExp(String("^SPDX-License-Identifier:\\sMIT$"), "");
    if (!lines.some((line) => spdxCopyrightRe.test(line))) {
      onError({
        "lineNumber": 1,
        "detail": "Missing `SPDX-FileCopyrightText`",
      });
    }
    if (!lines.some((line) => spdxLicensetRe.test(line))) {
      onError({
        "lineNumber": 1,
        "detail": "Missing `SPDX-License-Identifier: MIT`",
      });
    }
  }
}, {
  "names": [ "CHDR02", "front-page" ],
  "description": "There should be a link back to the front page",
  "tags": [ "color-and-hdr" ],
  "parser": "micromark",
  "function": function CHDR02(params, onError) {
    var lines = params.lines.slice();
    const emptyLine = function(line) {
      return line == "";
    };

    if (lines[0] == "Contents") {
      if (!emptyLine(lines[1]) ||
          !lines[2] == "[TOC]" ||
          !emptyLine(lines[3])) {
        onError({
          "lineNumber": 1,
          "detail": "Bad Table of Contents`",
        });
      }
      lines = lines.slice(4);
    }

    if (lines[0] != "[Front page](../README.md)") {
      onError({
        "lineNumber": 1,
        "detail": "Missing Front Page link",
      });
    }
  }
}, {
  "names": [ "CHDR03", "first-heading-h1" ],
  "description": "The first heading should be a h1",
  "tags": [ "color-and-hdr" ],
  "parser": "markdownit",
  "function": function CHDR03(params, onError) {
    const firstHeading =
      params.parsers.markdownit.tokens.find((token) => token.type === "heading_open");
    if (firstHeading.tag != "h1") {
      onError({
        "lineNumber": firstHeading.lineNumber,
        "detail": "First heading has tag " + firstHeading.tag,
      });
    }
  }
}];
