# color
[cC]olorimetry
[cC]olorimetric
[cC]olorimetrically
[tT]ristimulus
[lL]uma
[cC]hroma
[qQ]uantizer
xy
TF
OOTF
OETF
EOTF
Y'D'zD'x
OKLab

# graphics
[fF]ullscreen
[fF]ramebuffer
drm_[a-z]+
[mM]odesetting
viewporter
[sS]canout
[gG]amma
[dD]egamma
[sS]hader

# executables / programs / names
Xwayland
Wayland
Xorg
X11
proptest
colord
ddcutil
libdrm
drm_info
Matplotlib
Krita
Vulkan
OpenGL
Khronos
GitLab

# acronyms
LUT
PWL
LED
MR

# others
[bB]itstream
[pP]arametrization
[eE]ncodable
[dD]ecodable
[lL]inearize
[lL]inearization
[tT]oolkit
[wW]alkthrough

# probably mistakes that need fixing
standardish
adjustability
tunables
Screenshooting
unoccluded
(bpp|BPP)
# abbreviated
lum
const
# quoted from somewhere else
Dataformat
# Names
Bartosz
Poynton
Elle
Ciechanowski
